var HTTPS_PORT = 8443;

var fs = require('fs');
var https = require('https');
var path = require('path');

// Yes, SSL is required
var serverConfig = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};

// ----------------------------------------------------------------------------------------

// Create a server for the client html page
var handleRequest = function (request, response) {
    // Render the single client html file for any request the HTTP server receives
    console.log('request received: ' + request.url);

    if (request.url == '/') {
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.end(fs.readFileSync(__dirname + '/../client/index.html'));
    }
    else {
        if (path.extname(request.url)) {
            response.setHeader("Content-Type", allowed_extensions[path.extname(request.url)]);
            response.writeHead(200);
            response.end(fs.readFileSync(__dirname + '/../client' + request.url));
        }
        else {
            response.writeHead(200);
            response.end(fs.readFileSync(__dirname + '/../client/room.html'));
        }
    }
};

var httpsServer = https.createServer(serverConfig, handleRequest);
httpsServer.listen(HTTPS_PORT, '0.0.0.0');

console.log('Server started on https://localhost:8443');

allowed_extensions = {
    ".html": "text/html",
    ".js": "application/javascript",
    ".css": "text/css",
    ".txt": "text/plain",
    ".jpg": "image/jpeg",
    ".gif": "image/gif",
    ".png": "image/png",
    ".ico": "image/x-icon",
    ".less": "text/css"
};

var rooms = [];

var io = require('socket.io')(httpsServer);

io.on('connection', function (socket) {
    socket.emit('rooms', rooms);

    socket.on('wantToJoinRoom', function (room) {
        var scroom = io.sockets.adapter.rooms[room];
        if (scroom) {
            if (scroom.length == 1) { // this is the second connection to that room
                socket.join(room);
                socket.emit('second');
            }
        }
        else { // first connection
            rooms.push(room);
            socket.join(room);
            io.sockets.emit('rooms', rooms);
            socket.emit('first');
        }
    });

    socket.on('disconnect', function () {
        rooms.forEach(
            function (room) {
                if (!io.sockets.adapter.rooms[room]) {
                    rooms.splice(rooms.indexOf(room), 1);
                    io.sockets.emit('rooms', rooms);
                }
            }
        );
    });

    socket.on('ice', function (message) {
        socket.broadcast.to(message.room).emit('ice', message.ice);
    });

    socket.on('sdp', function (message) {
        socket.broadcast.to(message.room).emit('sdp', message.sdp);
    });

    socket.on('geo', function (message) {
        socket.broadcast.to(message.room).emit('geo', message.geo);
    })
});
