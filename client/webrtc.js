function getHost(link) {
    link = link.replace('//', '\\');
    var i = 0;
    while (link[i++] != '/') {
    }
    return link.replace('\\', '//').substring(0, i);
}

var socket = io.connect(getHost(window.location.href));

function getRoom(link) {
    var i = link.length;
    while (link[i--] != '/') {
    }
    return link.substring(i + 2);
}

var room = getRoom(window.location.href); // security issue

socket.emit('wantToJoinRoom', room);

socket.on('first', function () {
    initialize(false);
});

socket.on('second', function () {
    initialize(true);
});

var localVideo;
var remoteVideo;
var peerConnection;
var localStream;
var localGeo,
    remoteGeo;

var peerConnectionConfig = {
    'iceServers': [
        {'urls': 'stun:stun.services.mozilla.com'},
        {'urls': 'stun:stun.l.google.com:19302'}
    ]
};

function initialize(isCaller) {
    localVideo = document.getElementById('localVideo');
    remoteVideo = document.getElementById('remoteVideo');

    //serverConnection.onmessage = gotMessageFromServer;

    var constraints = {
        video: true,
        audio: false // set to true on "production" ;o
    };

    if (navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia(constraints).then(
            function (stream) {
                localStream = stream;
                localVideo.srcObject = stream;
                start(isCaller);
            }).catch(errorHandler);
    }
    else {
        alert('Your browser does not support getUserMedia API');
    }
}

function errorHandler(error) {
    console.log(error);
}

function start(isCaller) {
    peerConnection = new RTCPeerConnection(peerConnectionConfig);
    peerConnection.onicecandidate = gotIceCandidate;
    peerConnection.onaddstream = gotRemoteStream;
    peerConnection.addStream(localStream);

    if (isCaller) {
        peerConnection.createOffer().then(createdDescription).catch(errorHandler);
    }
}

function gotIceCandidate(event) {
    if (event.candidate != null) {
        socket.emit(
            'ice',
            {
                ice: event.candidate,
                room: room
            }
        );
    }
}

function gotRemoteStream(event) {
    remoteVideo.srcObject = event.stream;
    socket.emit(
        'geo',
        {
            geo: localGeo,
            room: room
        }
    );
}

function createdDescription(description) {
    peerConnection
        .setLocalDescription(description)
        .then(
            function () {
                socket.emit(
                    'sdp',
                    {
                        sdp: peerConnection.localDescription,
                        room: room
                    }
                );
            }
        )
        .catch(errorHandler);
}

socket.on(
    'sdp',
    function (sdp) {
        peerConnection
            .setRemoteDescription(new RTCSessionDescription(sdp))
            .then(
                function () {
                    // Only create answers in response to offers
                    if (sdp.type == 'offer') {
                        peerConnection.createAnswer().then(createdDescription).catch(errorHandler);
                    }
                }
            ).catch(errorHandler);
    }
);

socket.on(
    'ice',
    function (ice) {
        peerConnection.addIceCandidate(new RTCIceCandidate(ice)).catch(errorHandler);
    }
);

function showPosition(geoposition) {
    localGeo = {
        lat: geoposition.coords.latitude,
        long: geoposition.coords.longitude
    };
}

socket.on(
    'geo',
    function (geo) {
        remoteGeo = geo;
        var mymap = L.map('mapid', {zoomControl: true}).setView([localGeo.lat, localGeo.long], 18);
        mymap.zoomControl.setPosition('topright');

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            maxZoom: 20,
            id: 'noidgoten2.035l8ein',
            accessToken: 'pk.eyJ1Ijoibm9pZGdvdGVuMiIsImEiOiJjaW56c2M4Z2UwMHVodnhseXB4eWFuOXFjIn0.EzEykDYa9RmDQmWLxKqh1A'
        }).addTo(mymap);

        try {
            var local = L.marker([localGeo.lat, localGeo.long]).addTo(mymap);
            var remote = L.marker([remoteGeo.lat, remoteGeo.long]).addTo(mymap);
            local.bindPopup("<b>You</b>");
            remote.bindPopup("<b>Your friend</b>");
            var group = new L.featureGroup([local, remote]);
            mymap.fitBounds(group.getBounds());

            var popup = L.popup();

            function onMapClick(e) {
                popup
                    .setLatLng(e.latlng)
                    .setContent("You clicked the map at " + e.latlng.toString())
                    .openOn(mymap);
            }
            mymap.on('click', onMapClick);
        }
        catch(err){
            console.log(err);
        }
    }
);

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
}
else {
    alert("Geolocation is not supported by this browser.");
}