/**
 * Created by goten on 06/07/16.
 */

var socket = io.connect(window.location.href);

var localVideo = document.getElementById('localVideo');

var rooms = [];

var constraints = {
    video: true,
    audio: false
};

if (navigator.mediaDevices.getUserMedia) {
    navigator.mediaDevices.getUserMedia(constraints).then(
        function (stream) {
            localVideo.src = window.URL.createObjectURL(stream);
        }
    ).catch(errorHandler);
}
else {
    alert('Your browser does not support getUserMedia API');
}

function errorHandler(error) {
    console.log(error);
}

var choice = document.getElementById("join");

var connect = document.getElementById('connect');
connect.onclick = function () {
    var room = choice.options[choice.selectedIndex].value;
    if (room != 'Pick your room') {
        window.location.href += room
    }
    else {
        console.log('stahp');
    }
};

function alphanumeric(inputtxt) {
    var letterNumber = /^[0-9a-zA-Z]+$/;
    var res = inputtxt.match(letterNumber);
    if (res) {
        return res[0];
    }
    else {
        return false;
    }
}

document.getElementById('new').onclick = function () {
    var res = alphanumeric(document.getElementById('create').value);
    if (res) {
        window.location.href += res;
    }
    else {
        console.log('invalid input');
    }
};

socket.on(
    'rooms',
    function (res) {
        for (var i = 0; i < choice.options.length; ++i) {
            choice.remove(choice.options[i]);
        }
        choice.innerHTML = '<option disabled selected hidden>Pick your room</option>';
        rooms = res;
        rooms.forEach(
            function (room) {
                var option = document.createElement("option");
                option.text = room;
                choice.add(option);
            }
        )
    }
);