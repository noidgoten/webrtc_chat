# Installation
* npm install
* . generate_cert.sh
    * you can leave all the fields empty
* cd server
* node server/server.js  

# Description
* 2 user limit per room
* The server is needed only to establish the initial connection, afterwards the exchanges are independent

# Known bugs:
* Connection might get lost when refreshing/rejoining